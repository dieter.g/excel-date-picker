The Excel VBA Date Picker is a simple date picker solution for Excel.

This project uses no references and only standard controls and should work with any version of Excel.

![Screenshot](/Images/Screenshot.png?raw=true "How it Looks")